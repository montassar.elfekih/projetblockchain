App = {
  web3Provider: null,
  contracts: {},
  
  init: async function() {
    console.log("initWeb3 1");
    return await App.initWeb3();
  },

  initWeb3: async function() {
   // Modern dapp browsers...
   console.log("initWeb3 2");
    if (window.ethereum) {
      App.web3Provider = window.ethereum;
      try {
        // Request account access
        await window.ethereum.enable();
      } catch (error) {
        // User denied account access...
        console.error("User denied account access")
      }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
      App.web3Provider = window.web3.currentProvider;
    }
    // If no injected web3 instance is detected, fall back to Ganache
    else {
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    }
    console.log("initWeb3 3");
    web3 = new Web3(App.web3Provider);
    //App.initContract();
    App.initContractToken();
    App.initContractVerifDiplome();
    
  },


  initContractToken: function() {
    console.log("initContractToken");
    $.getJSON('TokenTunisie.json', function(data) {
      // Get the necessary contract artifact file and instantiate it with @truffle/contract
      var TokenTunisieArtifact = data;
      App.contracts.TokenTunisie = TruffleContract(TokenTunisieArtifact);
      //console.log("dataToken", data);
      // Set the provider for our contract
      App.contracts.TokenTunisie.setProvider(App.web3Provider);
      //App.contracts.TokenTunisie.mintMinerReward();
      // Use our contract to retrieve and mark the adopted pets
      //return App.markAdopted();
    });
  },

  initContractVerifDiplome: function() {
    console.log("initContractVerifDiplome");
    $.getJSON('VerifDiplome.json', function(data) {
      // Get the necessary contract artifact file and instantiate it with @truffle/contract
      var VerifDiplomeArtifact = data;
      App.contracts.VerifDiplome = TruffleContract(VerifDiplomeArtifact);
      //console.log("dataToken", data);
      // Set the provider for our contract
      App.contracts.VerifDiplome.setProvider(App.web3Provider);
      //App.contracts.TokenTunisie.mintMinerReward();
      // Use our contract to retrieve and mark the adopted pets
      //return App.markAdopted();
      App.login();
    });
  },

  login: function() {
    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }

      console.log("login function");
      var VerifDiplomeInstance;
      var account = accounts[0];
      var entre= true;
      App.contracts.VerifDiplome.deployed().then(function(instance) {
        VerifDiplomeInstance = instance;
        console.log("verif Entre");
        return VerifDiplomeInstance.getOneEntreprise(account); 
      }).then(function(result) {
        var card1 = document.getElementById("card1");
        card1.remove();
        var card2 = document.getElementById("card2");
        card2.remove();
        document.getElementById("panel").style.display = "block";
        var myobj = document.getElementById("univ");
        myobj.remove();
        document.getElementById("nomEn").textContent= "En tant que : "+result[0]+","+ result[1];
      }).catch(function(err) {
        console.log(err.message);
        entre= false;
      });
    if (entre){
      VerifDiplomeInstance.getOneEtablissement(account).then(function(result) {
        console.log("verif Etablissement");
        var card1 = document.getElementById("card1");
      card1.remove();
      var card2 = document.getElementById("card2");
     card2.remove();
     document.getElementById("panel").style.display = "block";
      var myobj1 = document.getElementById("ent");
     myobj1.remove();
        document.getElementById("nomUn").textContent= "En tant que : "+result[1]+" "+result[0]+ ","+ result[2];
      }).catch(function(err) {
        console.log(err.message);
        //entre= false;
      });
    }
    });
  },
  //0xc02cDf05014A2cf74dA2031Dc55A7F3aFA30cD26
  bindEventsAdd: function bindEvents() {
    console.log("event");
    $(document).on('click', '.evaluer', App.addToken());
    //$(document).on('click', '.verifier', App.burnToken());
  },
  bindEventsburn: function bindEvents() {
    console.log("event");
    //$(document).on('click', '.evaluer', App.addToken());
    $(document).on('click', '.verifier', App.verifDip());
  },

  bindEventsAddEtu: function bindEvents() {
    console.log("event");
    //$(document).on('click', '.evaluer', App.addToken());
    $(document).on('click', '.add-etu', App.handleAddEtudiant());
  },
  bindEventsLiaison: function bindEvents() {
    console.log("event");
    //$(document).on('click', '.evaluer', App.addToken());
    $(document).on('click', '.add-dip', App.liaisonDip());
  },
  bindEventsAddDip: function bindEvents() {
    console.log("event");
    //$(document).on('click', '.evaluer', App.addToken());
    $(document).on('click', '.add-dip', App.handleAddDiplome());
  },
  

addToken: function() {
  var amount =15;
  var toAddress = 0xD57aD75b10E003ba2F84fcb4F3AeDdE9c3c65734;
  var TokenTunisieInstance;

  web3.eth.getAccounts(function(error, accounts) {
    if (error) {
      console.log(error);
    }

    var account = accounts[0];
    console.log(account);

    App.contracts.TokenTunisie.deployed().then(function(instance) {
      TokenTunisieInstance = instance;
      //TokenTunisieInstance.burn(account, 50);
      var account0=  0xd22DCa5748e6135944743A7d75a8c16c18599351;
      //0xA5025Afb7fed32fbB39E33f021c604F99bAb866f
      console.log(account0);
      //TokenTunisieInstance.increaseAllowance(account, amount);
        return TokenTunisieInstance.mint(account, 15);
    }).then(function(result) {
      //console.log("x :",x);
      //alert('Transfer Successful!');
      //eturn App.handleAddEntreprise()
      App.evalEtu();
    }).catch(function(err) {
      console.log(err.message);
    });
  });
},

liaisonDip: function() {
  var VerifDiplomeInstance;

  web3.eth.getAccounts(function(error, accounts) {
    if (error) {
      console.log(error);
    }

    var account = accounts[0];
    var idD = parseInt($('#idDippL').val());
    var idEL = parseInt($('#idEL').val());
    //console.log(account);
    console.log("idD :",idD);
      console.log("note :",idEL);
    App.contracts.VerifDiplome.deployed().then(function(instance) {
      VerifDiplomeInstance = instance;
      
      return VerifDiplomeInstance.laison(idD, idEL); 
    }).then(function(result) {
        //alert('Transfer Successful!');
        console.log("laison :",result);
        return VerifDiplomeInstance.getOneDiplome(idD);
    }).then(function(result) {
        //alert('Transfer Successful!');
        console.log("etud :",result);
        document.getElementById("dipVerified").textContent= result[0] + " nom etudiant: "+result[1] + " id Etu: "+ result[2] + " type :"+ result[4];
        //document.getElementById("pay").textContent= result[1]; 
    }).catch(function(err) {
      console.log(err.message);
    });
  });
},
evalEtu: function() {
  var VerifDiplomeInstance;

  web3.eth.getAccounts(function(error, accounts) {
    if (error) {
      console.log(error);
    }

    var account = accounts[0];
    var idD = parseInt($('#idD').val());
    var note = parseInt($('#eval').val());
    //console.log(account);
    console.log("idD :",idD);
      console.log("note :",note);
    App.contracts.VerifDiplome.deployed().then(function(instance) {
      VerifDiplomeInstance = instance;
      
      return VerifDiplomeInstance.evaluer(idD, note); 
    }).then(function(result) {
      //alert('Transfer Successful!');
      console.log("AddEtudiant :",result);
      //currentID = result.a;
     return VerifDiplomeInstance.getcountEtu();
    }).then(function(result) {
        //alert('Transfer Successful!');
        console.log("countEtu :",result.c[0]);
        console.log("countEtu :",result.c[0]-1);
        //document.getElementById("nomEtu").textContent= result[1] + " "+ result[2] + " eval :"+ result[5];
        //document.getElementById("pay").textContent= result[1]; 
        return VerifDiplomeInstance.getOneEtudiant(result.c[0]-1);
    }).then(function(result) {
        //alert('Transfer Successful!');
        console.log("Entreprise :",result);
        document.getElementById("evalEtu").textContent= result[1] + " "+ result[2] + " eval :"+ result[3];
        //document.getElementById("pay").textContent= result[1]; 
    }).catch(function(err) {
      console.log(err.message);
    });
  });
},

verifDip: function() {
  var VerifDiplomeInstance;

  web3.eth.getAccounts(function(error, accounts) {
    if (error) {
      console.log(error);
    }

    var idDipp = parseInt($('#idDipp').val());
    var idE = parseInt($('#idE').val());
    //console.log(account);
    console.log("idD :",idDipp);
      console.log("note :",idE);
    App.contracts.VerifDiplome.deployed().then(function(instance) {
      VerifDiplomeInstance = instance;
      
      return VerifDiplomeInstance.verifierDiplome(idDipp, idE);
    }).then(function(result) {
        //alert('Transfer Successful!');
        console.log("verif  :",result);
        if (!result){
        App.burnToken(result);
          return;}
          App.burnToken(result);
          //document.getElementById("nomEtu").textContent= result[1] + " "+ result[2] + " eval :"+ result[5];
        //document.getElementById("pay").textContent= result[1]; 
        return VerifDiplomeInstance.getOneDiplome(idDipp);
    }).then(function(result) {
        //alert('Transfer Successful!');
        console.log("Diplome :",result);
        
        document.getElementById("verified").textContent= "True :"+result[4] + " "+ result[5]+ " : "+ result[1];
        //document.getElementById("pay").textContent= result[1]; 
    }).catch(function(err) {
      console.log(err.message);
       
    });
  });
},

burnToken: function(re) {
  var amount =15;
  var toAddress = 0xD57aD75b10E003ba2F84fcb4F3AeDdE9c3c65734;
  var TokenTunisieInstance;

  web3.eth.getAccounts(function(error, accounts) {
    if (error) {
      console.log(error);
    }

    var account = accounts[0];
    console.log(account);

    App.contracts.TokenTunisie.deployed().then(function(instance) {
      TokenTunisieInstance = instance;
      //TokenTunisieInstance.burn(account, 50);
      //var account0=  0xd22DCa5748e6135944743A7d75a8c16c18599351;
      //0xA5025Afb7fed32fbB39E33f021c604F99bAb866f
      //console.log(account0);
      
        //console.log("false", x);
        return TokenTunisieInstance.burn(account, 10);
    }).then(function(result) {
      //alert('Transfer Successful!');
      console.log("burn :",result);
      if (!re)
        document.getElementById("verified").textContent= "False"
      //document.getElementById("evalEtu").textContent= result[1] + " "+ result[2] + " eval :"+ result[3];
      //document.getElementById("pay").textContent= result[1]; 
  }).catch(function(err) {
      console.log(err.message);
    });
  });
},

handleAddEntreprise: function() {
  var VerifDiplomeInstance;

  web3.eth.getAccounts(function(error, accounts) {
    if (error) {
      console.log(error);
    }
    var nomEn = document.getElementById("NomEnt").value;
    var paysEnt = document.getElementById("paysEnt").value;
    var account = accounts[0];
    console.log(account);
    App.contracts.VerifDiplome.deployed().then(function(instance) {
      VerifDiplomeInstance = instance;
      
      return VerifDiplomeInstance.addEntreprise(1, nomEn,paysEnt); 
    }).then(function(result) {
      //alert('Transfer Successful!');
      console.log("AddEntreprise :",result);
     return VerifDiplomeInstance.getOneEntreprise(account);
    }).then(function(result) {
        //alert('Transfer Successful!');
        console.log("Entreprise :",result);
        var card1 = document.getElementById("card1");
        card1.remove();
        var card2 = document.getElementById("card2");
        card2.remove();
        document.getElementById("panel").style.display = "block";
        var myobj = document.getElementById("univ");
        myobj.remove();
        document.getElementById("nomEn").textContent= "En tant que : "+result[0]+","+ result[1];
        //document.getElementById("pay").textContent= result[1]; 
    }).catch(function(err) {
      console.log(err.message);
    });
  });
},

handleAddEtablissement: function() {
  var VerifDiplomeInstance;

  web3.eth.getAccounts(function(error, accounts) {
    if (error) {
      console.log(error);
    }

    var account = accounts[0];
    
    console.log(account);
    App.contracts.VerifDiplome.deployed().then(function(instance) {
      VerifDiplomeInstance = instance;
      //console.log("AddEntreprise ***", VerifDiplomeInstance);
      var nomUniv = document.getElementById("nomUniv").value;
    var typeUn = document.getElementById("typeUn").value;
    var paysUn = document.getElementById("paysUn").value;
    console.log("NomE :",nomUniv);
      console.log("typeUn :",typeUn);
      console.log("paysUn :",paysUn);
      return VerifDiplomeInstance.addEtablissement(1, nomUniv, typeUn, paysUn); 
    }).then(function(res) {
      //alert('Transfer Successful!');
      console.log("AddEntreprise :",res);
     return VerifDiplomeInstance.getOneEtablissement(account);
    }).then(function(result) {
        //alert('Transfer Successful!');
        console.log("Etabli :",result);
        var card1 = document.getElementById("card1");
      card1.remove();
      var card2 = document.getElementById("card2");
     card2.remove();
     document.getElementById("panel").style.display = "block";
      var myobj1 = document.getElementById("ent");
     myobj1.remove();
        document.getElementById("nomUn").textContent= "En tant que : "+result[1]+" "+result[0]+ ","+ result[2];
        //document.getElementById("pay").textContent= result[1]; 
    }).catch(function(err) {
      console.log(err.message);
      console.log(err);
    });
  });
},

handleAddEtudiant: function() {
  var VerifDiplomeInstance;
  var currentID;
  web3.eth.getAccounts(function(error, accounts) {
    if (error) {
      console.log(error);
    }

    var account = accounts[0];
    console.log(account);
    App.contracts.VerifDiplome.deployed().then(function(instance) {
      VerifDiplomeInstance = instance;
      var nom = document.getElementById("NomE").value;
      //parseInt($('#NomE').value);
      var prenom = document.getElementById("PrenomE").value;
      var idEtab = document.getElementById("idEtab").value;
      console.log("NomE :",nom);
      console.log("PrenomE :",prenom);
      console.log("idEtab :",idEtab);
      return VerifDiplomeInstance.addEtudiant(nom,prenom,idEtab); 
    }).then(function(result) {
      //alert('Transfer Successful!');
      console.log("AddEtudiant :",result);
      //currentID = result.a;
     return VerifDiplomeInstance.getcountEtu();
    }).then(function(result) {
        //alert('Transfer Successful!');
        console.log("countEtu :",result.c[0]);
        console.log("countEtu :",result.c[0]-1);
        //document.getElementById("nomEtu").textContent= result[1] + " "+ result[2] + " eval :"+ result[5];
        //document.getElementById("pay").textContent= result[1]; 
        return VerifDiplomeInstance.getOneEtudiant(result.c[0]-1);
    }).then(function(result) {
      //alert('Transfer Successful!');
      console.log("etudiant :",result);
      document.getElementById("nomEtu").textContent= result[1] + " "+ result[2] + " eval :"+ result[5]+" ,"+result[0];
      //document.getElementById("pay").textContent= result[1]; 
  }).catch(function(err) {
      console.log(err.message);
    });
  });
},

handleAddDiplome: function() {
  var VerifDiplomeInstance;

  web3.eth.getAccounts(function(error, accounts) {
    if (error) {
      console.log(error);
    }

    var account = accounts[0];
    console.log(account);
    App.contracts.VerifDiplome.deployed().then(function(instance) {
      VerifDiplomeInstance = instance;
      var type = document.getElementById("type").value;
      //parseInt($('#NomE').value);
      var idUn = document.getElementById("idUn").value;
      var nomEE = document.getElementById("nomEE").value;
      var spec = document.getElementById("spec").value;
      console.log("NomE :",idUn);
      console.log("PrenomE :",nomEE);
      console.log("idEtab :",spec);
      return VerifDiplomeInstance.addDiplome(nomEE,idUn, type, spec);   
    }).then(function(result) {
      //alert('Transfer Successful!');
      console.log("AddEntreprise :",result);
     return VerifDiplomeInstance.getcontDip();
    }).then(function(result) {
      //alert('Transfer Successful!');
      console.log("countEtu :",result.c[0]);
      console.log("countEtu :",result.c[0]-1);
      //currentID = result.a;
     return VerifDiplomeInstance.getOneDiplome(result.c[0]-1);
    }).then(function(result) {
        //alert('Transfer Successful!');
        console.log("Entreprise :",result);
        document.getElementById("dip").textContent= "Diplome Ajouté: "+result[4] + " "+ result[5]+ " : "+ result[1]+" ,"+result[0];
        //document.getElementById("pay").textContent= result[1]; 
    }).catch(function(err) {
      console.log(err.message);
    });
  });
},
};

$(function() {
  $(window).load(function() {
    App.init();
  });
  // $(document).getElementById("adopt").addEventListener("click", function() {
  //   alert("Hello World!");
  // });
});

// $(document.getElementById("adopt").addEventListener("click", function() {
//   alert("Hello World!");
// }));


//markAdopted: function() {
  //     var adoptionInstance;
  //     console.log("markAdopted");
  // App.contracts.Adoption.deployed().then(function(instance) {
  //   adoptionInstance = instance;
  //   console.log(adoptionInstance);
  //   return adoptionInstance.getAdopters.call();
  // }).then(function(adopters) {
  //   for (i = 0; i < adopters.length; i++) {
  //     if (adopters[i] !== '0x0000000000000000000000000000000000000000') {
  //       $('.panel-pet').eq(i).find('button').text('Success').attr('disabled', true);
  //     }
  //   }
  // }).catch(function(err) {
  //   console.log(err.message);
  //});
//    },


// handleAdopt: function(event) {
//   //event.preventDefault();
  
//   var petId = parseInt($(event.target).data('id'));
//   console.log("***",petId);
//   var adoptionInstance;
//   var tokenInstance;

// web3.eth.getAccounts(function(error, accounts) {
// if (error) {
//   console.log(error);
// }

// var account = accounts[0];
// console.log("account",account);
// // App.contracts.TokenTunisie.deployed().then(function(instance) {
// //   tokenInstance = instance;

// //   //Execute adopt as a transaction by sending account
// //   return tokenInstance..adopt(petId, {from: account});
// // }).then(function(result) {
// //   return App.markAdopted();
// // }).catch(function(err) {
// //   console.log(err.message);
// // });
// App.contracts.Adoption.deployed().then(function(instance) {
//   adoptionInstance = instance;
//   // Execute adopt as a transaction by sending account
//   return adoptionInstance.adopt(petId, {from: account});
// }).then(function(result) {
//   return App.markAdopted();
// }).catch(function(err) {
//   console.log(err.message);
// });
//   // }).catch(function(err) {
//   //   console.log(err.message);
//   // });
//   App.handleTransfer();  
// });


// },

//initContract: function() {
  //   console.log("initContract");
  //   $.getJSON('Adoption.json', function(data) {
  //     // Get the necessary contract artifact file and instantiate it with @truffle/contract
  //     var AdoptionArtifact = data;
  //     App.contracts.Adoption = TruffleContract(AdoptionArtifact);
  //     console.log("data", data);
  //     // Set the provider for our contract
  //     App.contracts.Adoption.setProvider(App.web3Provider);
    
  //     // Use our contract to retrieve and mark the adopted pets
  //     return App.markAdopted();
  //   });
  // },