// pragma solidity >=0.4.22 <0.8.0;

// import '@openzeppelin/contracts/crowdsale/Crowdsale.sol';
// import '@openzeppelin/contracts/crowdsale/emission/MintedCrowdsale.sol';
// import '@openzeppelin/contracts/token/ERC20/ERC20Mintable.sol';
// import './TokenTunisie.sol';

// contract TokenSale is Crowdsale, MintedCrowdsale{
    
//     constructor(
//         uint256 rate,
//         address payable wallet,
//         IERC20 token
//     )
//         Crowdsale(rate, wallet, token)
//         public
//     {

//     }
// }

// contract MyCrowdsaleDeployer {
//     constructor()
//         public
//     {
//         // create a mintable token
//         ERC20Mintable token = new TokenTunisie();

//         // create the crowdsale and tell it about the token
//         Crowdsale crowdsale = new TokenSale(
//             1,               // rate, still in TKNbits
//             msg.sender,      // send Ether to the deployer
//             token            // the token
//         );
//         // transfer the minter role from this contract (the default)
//         // to the crowdsale, so it can mint tokens
//         token.addMinter(address(crowdsale));
//         token.renounceMinter();
//     }
// }