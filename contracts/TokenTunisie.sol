pragma solidity >=0.4.22 <0.8.0;
 
//import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract TokenTunisie is  ERC20 {
    //decimals = 1;
     string public name;
    string public symbol;
    uint32 public decimals;
    constructor() public {
        symbol = "VD";
        name = "VerifDiplome";
        decimals = 0;
    }

    function mint(address account, uint256 amount) public{
        _mint(account, amount);
    }

    function burn(address account, uint256 amount) public{
        _burn(account, amount);
    }



 }