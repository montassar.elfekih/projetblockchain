pragma solidity >=0.4.22 <0.8.0;

contract Entreprise {
    uint64 public  id_En;
    string public nom_En;
    string public paysEn;
    
    

    constructor(uint64 id_dip1, string memory nom_etu1, string memory paysEn1) public  {
        id_En = id_dip1;
        nom_En = nom_etu1;
        paysEn = paysEn1;
    }
    
    
    function getNom()public view returns( string memory) {
        return nom_En;
        
    }
    
    function getpaysEn()public view returns( string memory) {
        return paysEn;
        
    }

}

contract Etablissement {
    uint64 public  id_etab;
    string public nom_etab;
    string public typeEtab;
    string public pays;
    //string public addressEtab;
    //string public sit_web;
    //uint public id_agent;
    
    

    constructor(uint64 id_etab1, string memory nom_etab1, string memory typeEtab1, string memory pays1) public  {
        id_etab = id_etab1;
        nom_etab = nom_etab1;
        typeEtab = typeEtab1;
        pays = pays1;
    }

    function getId()public view returns( uint64) {
        return id_etab;
        
    }

    function getNom()public view returns( string memory) {
        return nom_etab;
        
    }
    
    function getType()public view returns( string memory) {
        return typeEtab;
        
    }


    function getpaysE()public view returns( string memory) {
        return pays;
        
    }

}

contract Etudiant {
    uint public  id_etu;
    string public nom_etu;
    string public prenom_Etu;
    uint public id_etablissement;
    uint public id_entreprise;
    uint public evaluation;
    uint public id_diplome;
    

    constructor(uint id_etab1, string memory nom_etab1, string memory prenom_Etu1,
                uint id_etablissement1) public  {
        id_etu = id_etab1;
        nom_etu = nom_etab1;
        prenom_Etu = prenom_Etu1;
        id_etablissement = id_etablissement1;
        /*id_entreprise = id_entreprise1;
        evaluation = evaluation1;
        id_diplome= id_diplome1;*/
    }

     function getId()public view returns( uint) {
        return id_etu;
        
    }

    function getNom()public view returns( string memory) {
        return nom_etu;
        
    }
    
    function getPrenom()public view returns( string memory) {
        return prenom_Etu;
        
    }


    function getIdEtab()public view returns( uint) {
        return id_etablissement;
        
    }

    function getIdEn()public view returns( uint) {
        return id_entreprise;
        
    }

    function getIdDip()public view returns( uint) {
        return id_diplome;
        
    }
    function setIdDip(uint eval)public {
         id_diplome= eval;      
    }
    
    function getEval()public view returns( uint) {
        return evaluation;      
    }

    function setEval(uint eval)public {
         evaluation= eval;      
    }


}

contract Diplome {
    uint public  id_dip;
    string public nom_etu;
    uint public id_etu;
    uint public id_etablissement;
    string public typeDip;
    string public specialite;
    

    constructor(uint id_dip1, string memory nom_etu1,uint id_etablissement1, string memory typeDip1, string memory specialite1) public  {
        id_dip = id_dip1;
        nom_etu = nom_etu1;
        id_etablissement = id_etablissement1;
        typeDip = typeDip1;
        //id_etu = id_etu1;
        specialite = specialite1;
    }

    function getNomDip()public view returns( string memory) {
        return nom_etu;
        
    }
    
    function getType()public view returns( string memory) {
        return typeDip;
        
    }

    function getspecialite()public view returns( string memory) {
        return specialite;
        
    }


    function getIdDip()public view returns( uint) {
        return id_dip;
        
    }

    function getIdEtu()public view returns( uint) {
        return id_etu;
        
    }
    function setIdEtu(uint eval)public {
         id_etu= eval;      
    }
    
    function getIdEtab()public view returns( uint) {
        return id_etablissement;
        
    }

}

contract VerifDiplome {

        mapping (address => Entreprise) entrepriseStruct;
        address[] public entrepriseAddress;
        
        /*mapping (uint => Entreprise) entrepriseStructI;
        uint[] public entrepriseI;*/
        
        mapping (address => Etablissement) etablissementStruct;
        address[] public etablissementAddress;

        mapping (uint => Diplome) diplomeStruct;
        uint[] public diplomeAddress;
        uint public contDip=0;
        
        mapping (uint => Etudiant) etudiantStruct;
        uint[] public etudiantAddress;
        uint public countEtu=0;
        
    function getcontDip()public view returns( uint) {
        return contDip;
        
    }
    
    function getcountEtu()public view returns( uint) {
        return countEtu;
        
    }

    function addEntreprise( uint64  id_En,
        string memory nom_En,
        string memory paysEn) public {
         entrepriseStruct[msg.sender] = new Entreprise(id_En,nom_En,paysEn);
       entrepriseAddress.push(msg.sender);
       
    }

    function getOneEntreprise(address id)public view returns( string memory nom, string memory pays){
        nom =  entrepriseStruct[id].getNom();
        pays =  entrepriseStruct[id].getpaysEn();
    }

    function addEtablissement( uint64  id_E,
        string memory nom_E, string memory typeE,
        string memory paysE) public payable{
         etablissementStruct[msg.sender] = new Etablissement(id_E,nom_E,typeE, paysE);
         
       etablissementAddress.push(msg.sender);
       
    }

    function getOneEtablissement(address id)public view returns( string memory nom, string memory typeE, string memory pays){
        nom =  etablissementStruct[id].getNom();
        typeE =  etablissementStruct[id].getType();
        pays =  etablissementStruct[id].getpaysE();
    }

    function addDiplome(string memory nom_etu1,uint id_etu1,
      string memory typeDip1, string memory specialite1) public returns(uint a){
         diplomeStruct[contDip] = new Diplome(contDip,nom_etu1,id_etu1,typeDip1,specialite1);
        a = countEtu;
        diplomeAddress.push(contDip);
        contDip++;
        return a;
       //diplomeAddress.push(msg.sender);       
    }
    
    function laison(uint idDip,uint idEtu)public {
            etudiantStruct[idEtu].setIdDip(idDip);
            diplomeStruct[idDip].setIdEtu(idEtu);
    }
    

    function getOneDiplome(uint id)public view returns(uint id_dip1, string memory nom_etu1,uint id_etu1, 
    uint id_etablissement1,string memory typeDip1, string memory specialite1){
        id_dip1 =  diplomeStruct[id].getIdDip();
        nom_etu1 =  diplomeStruct[id].getNomDip();
        id_etu1 =  diplomeStruct[id].getIdEtu();
        id_etablissement1 =  diplomeStruct[id].getIdEtab();
        typeDip1 =  diplomeStruct[id].getType();
        specialite1 =  diplomeStruct[id].getspecialite();
    }


    function addEtudiant(string memory nom_etab1, string memory prenom_Etu1,
    uint id_etablissement1) public returns(uint a) {
         etudiantStruct[countEtu] = new Etudiant(countEtu,nom_etab1,prenom_Etu1, id_etablissement1);
         a = countEtu;
         countEtu++;
       etudiantAddress.push(countEtu);
       return a;
    }

    function evaluer(uint id,uint note)public {
            etudiantStruct[id].setEval(note);
            
    }

    function verifierDiplome (uint id, uint idEtu)public view returns(bool) {
        
        if(diplomeStruct[id].getIdEtu() == idEtu)
                    return true;
          
        return false;
    }
    
    function getOneEtudiant(uint id)public view returns(uint id_etab1, string memory nom_etab1, string memory prenom_Etu1,
        uint evaluation1, uint id_diplome1){
        id_etab1 =  etudiantStruct[id].getId();
        nom_etab1 =  etudiantStruct[id].getNom();
        prenom_Etu1 =  etudiantStruct[id].getPrenom();
        //id_etablissement1 =  etudiantStruct[id].getIdEtab();
        //id_entreprise1 =  etudiantStruct[id].getIdEn();
        evaluation1 =  etudiantStruct[id].getEval();
        id_diplome1 =  etudiantStruct[id].getIdDip();
    }


}

// contract Etablissement {
//     uint public  id_etab;
//     string public nom_etab;
//     string public typeEtab;
//     string public pays;
//     string public addressEtab;
//     string public sit_web;
//     uint public id_agent;

//     constructor(uint id_etab1, string memory nom_etab1, string memory typeEtab1, string memory pays1,
//         string memory addressEtab1,string memory sit_web1, uint id_agent1) public  {
//         id_etab = id_etab1;
//         nom_etab = nom_etab1;
//         typeEtab = typeEtab1;
//         pays = pays1;
//         addressEtab = addressEtab1;
//         sit_web = sit_web1;
//         id_agent = id_agent1;
//     }

// }

// contract Etudiant {
//     uint public  id_etu;
//     string public nom_etu;
//     string public prenom_Etu;
//     uint public id_etablissement;
//     uint public id_entrepris;
//     uint public evaluation;
//     uint public id_diplome;
    

//     constructor(uint id_etab1, string memory nom_etab1, string memory typeEtab1,
//                 uint id_agent1,  uint id_agent2, uint id_agent3, uint id_agent4) public  {
//         id_etu = id_etab1;
//         nom_etu = nom_etab1;
//         prenom_Etu = typeEtab1;
//         id_etablissement = id_agent1;
//         id_entrepris = id_agent2;
//         evaluation = id_agent3;
//         id_diplome= id_agent4;
//     }

// }

// contract Diplome {
//     uint public  id_dip;
//     string public nom_etu;
//     uint public id_etablissement;
//     string public typeDip;
    
    

//     constructor(uint id_dip1, string memory nom_etu1, uint id_etablissement1,  string memory typeDip1) public  {
//         id_dip = id_dip1;
//         nom_etu = nom_etu1;
//         id_etablissement = id_etablissement1;
//         typeDip = typeDip1;
        
//     }

// }

// contract Entreprise {
//     uint public  id_En;
//     string public nom_En;
//     string public paysEn;
    
    

//     constructor(uint id_dip1, string memory nom_etu1, string memory paysEn1) public  {
//         id_En = id_dip1;
//         nom_En = nom_etu1;
//         paysEn = paysEn1;
//     }

// }

// import "./TokenTunisie.sol";

// contract VerifDiplome is TokenTunisie{

//     struct Entreprise{
//         uint  id_En;
//         string nom_En;
//         string paysEn;
//     }

//     mapping (uint => Entreprise) entrepriseStruct;
//         address[] public entrepriseAddress;

//     Entreprise[] entreprises;    
//     struct Diplome {
//         uint   id_dip;
//         string  nom_dip;
//         uint id_etablissement;
//         string typeDip;
//     }
//      mapping (address => Diplome) diplomeStruct;
//         address[] public diplomeAddress;

//     struct Etudiant {
//         uint   id_etu;
//         string nom_etu;
//         string prenom_Etu;
//         uint id_etablissement;
//         uint id_entrepris;
//         uint evaluation;
//         uint id_diplome;
//     }

//     mapping (address => Etudiant) etudiantStruct;
//         address[] public etudiantAddress;

//     struct Etablissement {
//         uint   id_etab;
//         string  nom_etab;
//         string  typeEtab;
//         string pays;
//         string  addressEtab;
//         string  sit_web;
//         uint id_agent;
//     }

//     mapping (address => Etablissement) etablissementStruct;
//     address[] public etablissementAddress;
    
    
//     function addEtablissement(uint id_etab, string memory nom_etab,string memory typeEtab, string memory pays,string memory addressEtab,string memory sit_web, uint id_agent) public {
//          etablissementStruct[msg.sender].id_etab = id_etab;
//          etablissementStruct[msg.sender].nom_etab = nom_etab;
//          etablissementStruct[msg.sender].typeEtab = typeEtab;
//          etablissementStruct[msg.sender].pays = pays;
//         etablissementStruct[msg.sender].addressEtab = addressEtab;
//         etablissementStruct[msg.sender].sit_web = sit_web;
//         etablissementStruct[msg.sender].id_agent = id_agent;

//         etablissementAddress.push(msg.sender);
//     }

//     function addEtudiant(uint id_etu,
//         string memory nom_etu,
//         string memory prenom_Etu,
//         uint id_etablissement,
//         uint id_entrepris,
//         uint evaluation,
//         uint id_diplome) public {
//          etudiantStruct[msg.sender].id_etu = id_etu;
//          etudiantStruct[msg.sender].nom_etu = nom_etu;
//          etudiantStruct[msg.sender].prenom_Etu = prenom_Etu;
//          etudiantStruct[msg.sender].id_etablissement = id_etablissement;
//         etudiantStruct[msg.sender].id_entrepris = id_entrepris;
//         etudiantStruct[msg.sender].evaluation = evaluation;
//         etudiantStruct[msg.sender].id_diplome = id_diplome;

//         etudiantAddress.push(msg.sender);
//     }

//     function addDiplome(uint id_dip,
//         string memory nom_dip,
//         uint id_etablissement,
//         string memory typeDip) public {
//          diplomeStruct[msg.sender].id_dip = id_dip;
//          diplomeStruct[msg.sender].nom_dip = nom_dip;
//          diplomeStruct[msg.sender].id_etablissement = id_etablissement;
//          diplomeStruct[msg.sender].typeDip = typeDip;

//         diplomeAddress.push(msg.sender);
//     }

//     function addEntreprise( uint  id_En,
//         string memory nom_En,
//         string memory paysEn) public {
//          entrepriseStruct[msg.sender].id_En = id_En;
//          entrepriseStruct[msg.sender].nom_En = nom_En;
//          entrepriseStruct[msg.sender].paysEn = paysEn;

//         entrepriseAddress.push(entrepriseStruct);
//     }

//     function getDiplome(address id)public view returns(address[] memory){
//         return diplomeAddress;
//     }

//     function getAllEntreprise()public view returns(address[] memory){
//         return entrepriseAddress;
//     }

    

//     // function getOneEntreprise(address memory id)public view returns(address memory){

//     //     // id_En1 = entrepriseAddress[id].entrepriseStruct.id_En;
//     //     // nom_En1 = entrepriseAddress[id].entrepriseStruct.nom_En1;
//     //     // paysEn1 = entrepriseAddress[id].entrepriseStruct.paysEn1;
//     //     return entrepriseAddress[id];
//     // }

//     function getAllEtudiant()public view returns(address[] memory){
//         return etudiantAddress;
//     }

//     function getAllEtablissement()public view returns(address[] memory){
//         return etablissementAddress;
//     }


// }